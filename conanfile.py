from conans import ConanFile
from conans import tools

import os
from os import path


class WafInstaller(ConanFile):
    name = "waf"
    version = "0.1.2"
    license = "MIT"
    url = "https://gitlab.com/no-face/waf-conan.git"
    options = {"version": ["1.9.7", "1.9.14"]}
    default_options = "version=1.9.14"
    build_policy="missing"
    
    description="Package to simplify installing the waf build system (https://waf.io/) binary in conan packages"

    def build(self): # Load file on build because it will change for each option
        bin_url = "https://waf.io/waf-%s" % self.options.version
        self.output.info('Downloading waf from: "{}"'.format(bin_url))
        tools.download(bin_url, "waf")
        self.add_exec_permission("waf")

    def package(self):
        self.copy("waf", src="", dst=".")

    def package_info(self):
        # Adds to path waf installation folder
        self.env_info.path.append(self.package_folder)
        
        self.cpp_info.includedirs = []  
        self.cpp_info.libdirs = [] 
        self.cpp_info.resdirs = []

    def add_exec_permission(self, f):
        import stat
        
        current_permissions = stat.S_IMODE(os.lstat(f).st_mode)
        os.chmod(f, current_permissions | stat.S_IXUSR)
